GitLab Group Audit
------------------

These scripts give the ability to query a top level GitLab group
and get information about all of the projects under it. This is handy
for regular audits to clean-up incorrect permissions, old deploy keys,
and projects.

Running each command with the `-h` flag will print a help message
with the required and optional parameters to use in each.

Scripts
======
* [Audit Members](audit_members)
* [Audit Projects](audit_projects)
* [Audit Deploy Keys](audit_deploy_keys)
