#!/usr/bin/python3

'''
Generic commands that get data from the GitLab API and return it
'''
import requests

def get_items_from_api(url, params):
    '''
    Calls the given URL with the provided parameters,
    looping over all of the pages of results.
    args:
        url (str): GitLab API url endpoint
        params (dict): Request parameters
    returns:
        (list of dict): Results from the API call
    '''
    items = []
    params['page'] = 1
    while True:
        req = requests.get(url, params)
        if 'X-Page' not in req.headers or 'X-Total-Pages' not in req.headers:
            return req.json()
        items.extend(req.json())
        if int(req.headers['X-Page']) >= int(req.headers['X-Total-Pages']):
            break
        params['page'] = req.headers['X-Next-Page']

    return items

def get_access_level(access_level_id):
    '''
    Map the access level id to the human-readable value
    https://docs.gitlab.com/ee/api/access_requests.html#valid-access-levels
    args:
        access_level_id (int): Numeric access level returned from the API
    returns:
        (str|None): String representation of the access level
    '''
    levels = {
            0:"No access",
            5: "Minimal access",
            10: "Guest",
            20: "Reporter",
            30: "Developer",
            40: "Maintainer",
            50: "Owner"
    }
    if access_level_id in levels:
        return levels[access_level_id]
    else:
        return None

def get_deploy_keys_in_project(project_id, api_url, default_params):
    '''
    Get all of the deploy keys for a given project
    args:
        project_id (int): project id to query
        api_url (str): Base GitLab API endpoint
        default_params (dict): Default parameters to pass to the API
    returns:
        (list of dict): Deploy keys
    '''
    return get_items_from_api(
            f"{api_url}/projects/{project_id}/deploy_keys",
            default_params)

def get_project_pipelines(project_id, api_url, default_params):
    '''
    Get all of the proects for a given group
    args:
        group_id (int): group id to query
        api_url (str): Base GitLab API endpoint
        default_params (dict): Default parameters to pass to the API
    returns:
        (list of dict): Projects
    '''
    return get_items_from_api(
            f"{api_url}/projects/{project_id}/pipelines",
            default_params)

def get_projects_in_group(group_id, api_url, default_params):
    '''
    Get all of the proects for a given group
    args:
        group_id (int): group id to query
        api_url (str): Base GitLab API endpoint
        default_params (dict): Default parameters to pass to the API
    returns:
        (list of dict): Projects
    '''
    return get_items_from_api(
            f"{api_url}/groups/{group_id}/projects",
            default_params)

def get_subgroups_in_group(group_id, api_url, default_params):
    '''
    Get all of the sub-groups within a group
    args:
        group_id (int): group id to query
        api_url (str): Base GitLab API endpoint
        default_params (dict): Default parameters to pass to the API
    returns:
        (list of dict): Groups
    '''
    return get_items_from_api(
            f"{api_url}/groups/{group_id}/descendant_groups",
            default_params)

def get_group(group_id, api_url, default_params):
    '''
    Get information about a group
    args:
        group_id (int): group id to query
        api_url (str): Base GitLab API endpoint
        default_params (dict): Default parameters to pass to the API
    returns:
        (dict): Group info
    '''
    return get_items_from_api(
            f"{api_url}/groups/{group_id}",
            default_params)

def get_group_members(group_id, api_url, default_params):
    '''
    Get all of the members within a group, including inheritted ones
    args:
        group_id (int): group id to query
        api_url (str): Base GitLab API endpoint
        default_params (dict): Default parameters to pass to the API
    returns:
        (list of dict): Group memebers
    '''
    return get_items_from_api(
            #f"{api_url}/groups/{group_id}/members/all",
            f"{api_url}/groups/{group_id}/members",
            default_params)

def get_project_members(project_id, api_url, default_params):
    '''
    Get all of the members within a project, including inheritted ones
    args:
        project_id (int): project id to query
        api_url (str): Base GitLab API endpoint
        default_params (dict): Default parameters to pass to the API
    returns:
        (list of dict): Project memebers
    '''
    return get_items_from_api(
            #f"{api_url}/projects/{project_id}/members/all",
            f"{api_url}/projects/{project_id}/members",
            default_params)
