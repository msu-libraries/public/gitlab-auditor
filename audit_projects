#!/usr/bin/python3

'''
For the top level group ID provided, query all projects and
return information about them.

'''
import argparse
import requests
import gitlab_api
import json

def print_projects(projects, csv=False):
    '''
    Print the projects in a csv format
    args:
        projects (list of dict): Projects to print
        csv (bool): If it should display in CSV form
    '''
    if csv:
        # Print the header row
        print("id, name, namespace, creator_id, created_at, last_activity_at, empty_repo, visibility")

    # Print the data rows
    for project in projects:
        print_project(project, csv)

def print_project(project, csv=False):
    '''
    Print a project's information
    args:
        project (dict): Project to display
        csv (bool): If it should display in CSV form
    '''
    if csv:
        print(f"{project['id']}, {project['name']}, {project['name_with_namespace']}, ",
            f"{project['creator_id']}, {project['created_at']}, {project['last_activity_at']}, ",
            f"{project['empty_repo']}, {project['visibility']}")
    else:
        print(f"Project: {project['name_with_namespace']} ({project['id']}) ",
            f"Created At: {project['created_at']} Last Activity: {project['last_activity_at']} ",
            f"Empty Repo? {project['empty_repo']} Visibility: {project['visibility']}")


def get_projects(url, group_id, params, verbose, projects=[]):
    '''
    Function that recursivly processes all projects within
    groups or sub-groups of the given top level group.
    args:
        url (str): GitLab API endpoint
        group_id (int): Top level group to start processing from
        params (dict): Default parameters to pass to the API
            such as the personal_token
        verbose (bool): If the function should print where it is
            in the processing
        deploy_keys (list of dict): Initial set of projects
            which defaults to an empty list. This is used for the
            recursive calls
    returns:
        (list of dict): The projects found
    '''
    # Get info for the group
    group = gitlab_api.get_group(group_id, url, params)

    if verbose:
        print(f"Finding projects for group: {group['name']} ({group_id})")

    # For each project in the group, add it's deploy keys to the list
    projects.extend(gitlab_api.get_projects_in_group(group_id, url, params))

    if verbose:
        print(f"Checking for sub-groups in group {group['name']} ({group_id})")

    # For each sub-group within the group, call this function to get
    # its list of projects
    for group in gitlab_api.get_subgroups_in_group(group_id, url, params):
        projects = get_projects(url, group['id'], params, verbose, projects)

    return projects

if __name__ == '__main__':
    # Parse the command line arguments
    desc = """
    Audit GitLab projects in a group

    example:
        ./audit_projects -g 123 -t my_api_token --print-text
    """
    parser = argparse.ArgumentParser(description=desc, formatter_class=argparse.RawTextHelpFormatter)
    requiredNamed = parser.add_argument_group('required named arguments')
    requiredNamed.add_argument('-t', '--token', help='API token to use for credentials', required=True)
    requiredNamed.add_argument('-g', '--group', type=int, help='Group ID in GitLab to audit', required=True)
    parser.add_argument('-a', '--api-url', help='Default: https://gitlab.msu.edu/api/v4', default='https://gitlab.msu.edu/api/v4')
    parser.add_argument('-p', '--print-text', help='Print keys as text', action="store_true")
    parser.add_argument('-c', '--print-csv', help='Print keys as CSV', action="store_true")
    parser.add_argument('-v', '--verbose', help='Verbose printing during processing', action="store_true")
    args = vars(parser.parse_args())

    # Set the default request parameters to the GitLab API
    params = {'private_token':args['token'],
            'simple':'false',
            'per_page':20,
            'page':1}

    # Check flags
    if not args['print_csv'] and not args['print_text']:
        print("Provide at least one of these flags: --print-csv, --print-text")
        exit(0)

    # Get the deploy keys
    projects = get_projects(args['api_url'], args['group'], params, args['verbose'],[])

    # Process flags
    if args['print_csv']:
        print_projects(projects, csv=True)
    if args['print_text']:
        print_projects(projects, csv=False)
